import plugin from './vue-plugin.js'
import _mixins from './mixins.js'

export const VueFeathers = plugin
export const mixins = _mixins

export default {
  VueFeathers,
  mixins,
}
